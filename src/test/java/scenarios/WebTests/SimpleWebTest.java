package scenarios.WebTests;

import org.testng.Assert;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import setup.Driver;

@Test(groups = "web")
public class SimpleWebTest extends Driver {

    @Test(description = "Simple test which opens iana.org", groups = "web")
    public void ConnectToIanaTest() throws Exception {
        driver().get(SUT);
        driverWait().until(ExpectedConditions.urlToBe(SUT + "/"));
        Assert.assertEquals(driver().getTitle(), "Internet Assigned Numbers Authority");
    }

}
