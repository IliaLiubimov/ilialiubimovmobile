package scenarios.NativeTests;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Driver;

@Test(groups = "native")
public class SimpleNativeTest extends Driver {

    public void SimpleContactManagerTest() throws Exception {
        String appPackageName = "com.example.android.contactmanager:id/";
        By addContactButton = By.id(appPackageName + "addContactButton");
        By accountSpinned = By.id(appPackageName + "accountSpinner");
        By contactName = By.id(appPackageName + "contactNameEditText");
        By contactPhone = By.id(appPackageName + "contactPhoneEditText");

        driver().findElement(addContactButton).click();
        driverWait().until(ExpectedConditions.visibilityOfElementLocated(accountSpinned));
        Assert.assertNotNull(driver().findElements(accountSpinned));
        Assert.assertNotNull(driver().findElements(contactName));
        Assert.assertNotNull(driver().findElements(contactPhone));
        Assert.assertTrue(driver().isKeyboardShown());
    }
}
