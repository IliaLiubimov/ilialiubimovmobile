package scenarios;

import org.testng.annotations.*;
import setup.AppPropertyNames;
import setup.Driver;


public class Hooks extends Driver {

    @BeforeSuite(groups = "web")
    public void SetUpWeb() throws Exception {
        setAppPropertyName(AppPropertyNames.WEB_PROPERTY);
        prepareDriver();
    }

    @BeforeSuite(groups = "native")
    public void SetUpNative() throws Exception {
        setAppPropertyName(AppPropertyNames.NATIVE_PROPERTY);
        prepareDriver();
    }

    @AfterSuite(description = "Close driver on all test completion")
    public void tearDown() throws Exception {
        driver().quit();
    }
}
