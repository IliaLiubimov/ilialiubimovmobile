package setup;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class TestProperties {

    protected static String PROP_NAME;

    Properties properties = new Properties();

    protected void setAppPropertyName(AppPropertyNames appName) {
        PROP_NAME = appName.getPropertyName();
    }

    Properties getProperties() throws IOException {
        FileInputStream in = new FileInputStream(System.getProperty("user.dir") + "/" + PROP_NAME);
        properties.load(in);
        in.close();
        return properties;
    }

    protected String getProp(String propKey) throws IOException {
        if (!properties.containsKey(propKey)) properties = getProperties();
        return properties.getProperty(propKey, null);
    }
}
