package setup;

public enum AppPropertyNames {
    WEB_PROPERTY("webTest.properties"),
    NATIVE_PROPERTY("nativeTest.properties");

    private String propertyName;

    AppPropertyNames(final String name) {
        this.propertyName = name;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
