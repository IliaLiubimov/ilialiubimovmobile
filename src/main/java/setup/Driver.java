package setup;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;

public class Driver extends TestProperties {

    private static AndroidDriver driverSingleton = null; //necessary for keyboard check
    private static WebDriverWait webDriverWaitSingleton;
    protected DesiredCapabilities capabilities;

    protected static String AUT;
    protected static String SUT;
    protected static String TEST_PLATFORM;
    protected static String DRIVER;

    protected void prepareDriver() throws Exception {

        AUT = getProp("aut");
        String t_sut = getProp("sut");
        SUT = t_sut == null ? null : "https://" + t_sut;
        TEST_PLATFORM = getProp("platform");
        DRIVER = getProp("driver");

        capabilities = new DesiredCapabilities();
        String browserName;

        if ("Android".equals(TEST_PLATFORM)) {
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "MUENW18C14006551");
            browserName = "Chrome";
        } else if ("iOS".equals(TEST_PLATFORM)) {
            browserName = "Safari";
        } else {
            throw new Exception("Unknown Mobile Platform");
        }

        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, TEST_PLATFORM);

        if (AUT != null && SUT == null) {
            File app = new File(AUT);
            capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        } else if (SUT != null && AUT == null) {
            capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, browserName);
        } else throw new Exception("Unclear type of mobile app");

        if (driverSingleton == null) driverSingleton = new AndroidDriver(new URL(DRIVER), capabilities);
        if (webDriverWaitSingleton == null) webDriverWaitSingleton = new WebDriverWait(driver(), 10);

    }

    protected AndroidDriver driver() throws Exception {
        if (driverSingleton == null) prepareDriver();
        return driverSingleton;
    }

    protected WebDriverWait driverWait() throws Exception {
        return webDriverWaitSingleton;
    }

}
